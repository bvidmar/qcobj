# QCOBJ #
QCOBJ a Python package to handle quantity-aware configuration files

## In short ##
A Python package to help scientists and researchers to add physical quantities
to the configuration files they use in their computer programs.
Highlights:

* Parameter values are validated against user defined specifications to
  ensure they are in the correct range and eventually converted to the
  requested unit of measurement. 

* A graphical user interface to display and modify configuration file content
  is included.

* More than two configuration files can be compared highlighting the
  differences between them.

### Install QCOBJ ###

QCOBJ can be installed without the graphical user interface:

    pip3 install git+https://bitbucket.org/bvidmar/qcobj

or with the GUI:

    pip3 install git+https://bitbucket.org/bvidmar/qcobj[with_pyside2]

or

    pip3 install git+https://bitbucket.org/bvidmar/qcobj[with_pyqt5]

QCOBJ was published on
[SoftwareX, Volume 7, January–June 2018, Pages 347-351](
https://www.sciencedirect.com/science/article/pii/S2352711018302383).

### Where is the documentation? ###

[Here!](https://bvidmar.bitbucket.io/qcobj/)

### Who do I talk to? ###

* [Roberto Vidmar](mailto:rvidmar@inogs.it)

* [Nicola Creati](mailto:ncreati@inogs.it)
