from pygments.lexer import RegexLexer, bygroups
from pygments.token import (Text, Comment, Keyword, Name, Operator, String,
        Error, Number, Generic, Whitespace)
from pygments.style import Style

# =============================================================================
class CfgLexer(RegexLexer):
    """ Lexer for configuration files in INI style.
    """
    name = 'CFG'
    aliases = ['cfg', ]
    filenames = ['*.cfg', ]
    mimetypes = ['text/x-cfg', ]

    tokens = {
        'root': [
                (r'\s+', Text),
                (r'#.*?$', Comment),
                (r'\[.*?\]$', Keyword),
                (r'(.*?)(\s*)(=)(\s*)(.*?)$',
                bygroups(Name.Attribute, Text, Operator, Text, String)),
                ]

        }

# =============================================================================
class CfgStyle(Style):
    """ A style for the HTML formatter used to format CFG files
    """
    default_style = ''

    styles = {
        Whitespace:             '#bbbbbb',

        Comment:                'italic #008800',
        Comment.Preproc:        'noitalic #008080',
        Comment.Special:        'noitalic bold',

        String:                 '#0000FF',
        String.Char:            '#800080',
        Number:                 '#0000FF',
        Keyword:                'bold #000080',
        Operator.Word:          'bold',
        Name.Tag:               'bold #000080',
        Name.Attribute:         '#FF0000',

        Generic.Heading:        '#999999',
        Generic.Subheading:     '#aaaaaa',
        Generic.Deleted:        'bg:#ffdddd #000000',
        Generic.Inserted:       'bg:#ddffdd #000000',
        Generic.Error:          '#aa0000',
        Generic.Emph:           'italic',
        Generic.Strong:         'bold',
        Generic.Prompt:         '#555555',
        Generic.Output:         '#888888',
        Generic.Traceback:      '#aa0000',

        # Hack to show multiline strings
        Error:                  '#0000FF',
    }

