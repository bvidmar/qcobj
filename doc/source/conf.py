#
# QCOBJ documentation build configuration file, created by
# sphinx-quickstart on Fri Nov  9 13:40:36 2012.
#
# This file is execfile()d with the current directory set to its containing
# dir.
#
# Note that not all possible configuration values are present in this
# autogenerated file.
#
# All configuration values have a default; values that are commented out
# serve to show the default.

import os
import sys
sys.path.insert(0, os.path.abspath('../..'))
sys.path.insert(0, os.path.abspath('.'))
from sphinx.highlighting import lexers

# Local imports
import qcobj
from cfglexer import CfgLexer, CfgStyle


lexers['cfg'] = CfgLexer

# General information about the project.
project = 'QCOBJ'
copyright = '2017-2021, Roberto Vidmar, Nicola Creati'
# The full version, including alpha/beta/rc tags.
release = qcobj.__version__

def pygments_monkeypatch_style(mod_name, cls):
    import sys
    import pygments.styles
    cls_name = cls.__name__
    mod = type(__import__("os"))(mod_name)
    setattr(mod, cls_name, cls)
    setattr(pygments.styles, mod_name, mod)
    sys.modules["pygments.styles." + mod_name] = mod
    from pygments.styles import STYLE_MAP
    STYLE_MAP[mod_name] = mod_name + "::" + cls_name


pygments_monkeypatch_style("cfg_style", CfgStyle)
pygments_style = "cfg_style"

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom ones.
extensions = [
        #'sphinx.ext.intersphinx',
        'sphinx.ext.autodoc',
        'sphinx.ext.mathjax',
        'sphinx.ext.viewcode',

        'sphinx.ext.todo',
        'sphinx.ext.coverage',
        'sphinx.ext.inheritance_diagram',
        #'sphinx_automodapi.automodapi',
        #'sphinx_automodapi.smart_resolver',
        #'qcobjLexer',
        'sphinxcontrib.napoleon']
# Automodapi settings
#automodsumm_inherited_members = True

todo_include_todos = True
inheritance_graph_attrs = dict(rankdir="TB")
#inheritance_graph_attrs = dict(rankdir="TB", size='"10.0, 20.0"',
  #ratio='compress', fontsize=12)
inheritance_node_attrs = dict(fontname='Arial', fontsize=12,
        colorscheme='accent8', style='filled')
# Napoleon settings
napoleon_google_docstring = True
napoleon_numpy_docstring = True
#napoleon_include_init_with_doc = False
napoleon_include_init_with_doc = True
#napoleon_include_private_with_doc = False
napoleon_include_private_with_doc = True
#napoleon_include_special_with_doc = False
napoleon_include_special_with_doc = True
napoleon_use_admonition_for_examples = False
napoleon_use_admonition_for_notes = False
napoleon_use_admonition_for_references = False
napoleon_use_ivar = False
napoleon_use_param = True
napoleon_use_rtype = True
napoleon_use_keyword = True

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# The suffix of source filenames.
source_suffix = '.rst'

# The language for content autogenerated by Sphinx. Refer to documentation
# for a list of supported languages.
#language = None

# There are two options for replacing |today|: either, you set today to some
# non-false value, then it is used:
#today = ''
# Else, today_fmt is used as the format for a strftime call.
#today_fmt = '%B %d, %Y'

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
exclude_patterns = []

# The reST default role (used for this markup: `text`) to use for all
# documents.
#default_role = None

# If true, '()' will be appended to :func: etc. cross-reference text.
add_function_parentheses = True

# If true, the current module name will be prepended to all description
# unit titles (such as .. function::).
add_module_names = True

# If true, sectionauthor and moduleauthor directives will be shown in the
# output. They are ignored by default.
#show_authors = False

# A list of ignored prefixes for module index sorting.
#modindex_common_prefix = []


# -- Options for HTML output --------------------------------------------------
html_theme = 'default'
ogs_link_color = "blue"
html_theme_options = {
        "sidebarbgcolor": "lightgray",
        "sidebartextcolor": "black",
        "sidebarlinkcolor": ogs_link_color,
        "stickysidebar": True,

        "footerbgcolor": "lightgray",
        "footertextcolor": "black",

        "relbarbgcolor": "lightgray",
        "relbartextcolor": "black",
        "relbarlinkcolor": ogs_link_color,
        "externalrefs": True,

        "bgcolor": "#E0E0E0",
        "visitedlinkcolor": "indigo",
        }
#html_theme = 'sphinxdoc'
#html_theme = 'scrolls'
#html_theme = 'agogo'
#html_theme = 'nature'

html_style = 'default.css'

# A shorter title for the navigation bar.  Default is the same as html_title.
#html_short_title = None

# The name of an image file (relative to this directory) to place at the top
# of the sidebar.
#html_logo = None
html_logo = "images/ogs.png"

# The name of an image file (within the static path) to use as favicon of the
# docs.  This file should be a Windows icon file (.ico) being 16x16 or 32x32
# pixels large.
#html_favicon = None

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']

# If not '', a 'Last updated on:' timestamp is inserted at every page bottom,
# using the given strftime format.
#html_last_updated_fmt = '%b %d, %Y'

# If true, SmartyPants will be used to convert quotes and dashes to
# typographically correct entities.
#html_use_smartypants = True

# Custom sidebar templates, maps document names to template names.
html_sidebars = {
        '**': ['localtoc.html',
               'globaltoc.html',
               'relations.html',
               'sourcelink.html',
               'searchbox.html',
              ]
        }

# Additional templates that should be rendered to pages, maps page names to
# template names.
#html_additional_pages = {}

# If false, no module index is generated.
#html_domain_indices = True

# If false, no index is generated.
html_use_index = True

# If true, the index is split into individual pages for each letter.
#html_split_index = False

# If true, links to the reST sources are added to the pages.
#html_show_sourcelink = True

# If true, "Created using Sphinx" is shown in the HTML footer. Default is True.
#html_show_sphinx = True

# If true, "(C) Copyright ..." is shown in the HTML footer. Default is True.
#html_show_copyright = True

# If true, an OpenSearch description file will be output, and all pages will
# contain a <link> tag referring to it.  The value of this option must be the
# base URL from which the finished HTML is served.
#html_use_opensearch = ''

# This is the file name suffix for HTML files (e.g. ".xhtml").
#html_file_suffix = None

# -- Options for LaTeX output -------------------------------------------------

latex_elements = {
# The paper size ('letterpaper' or 'a4paper').
'papersize': 'a4paper',

# The font size ('10pt', '11pt' or '12pt').
#'pointsize': '10pt',

# Additional stuff for the LaTeX preamble.
#'preamble': '',
}

# Grouping the document tree into LaTeX files. List of tuples
# (source start file, target name, title, author, documentclass
# [howto/manual]).
latex_documents = [
  ('index', 'QCOBJ.tex', u'QCOBJ Documentation',
   u'Roberto Vidmar, Nicola Creati', 'manual'),
]

# The name of an image file (relative to this directory) to place at the top of
# the title page.
#latex_logo = None

# For "manual" documents, if this is true, then toplevel headings are parts,
# not chapters.
#latex_use_parts = False

# If true, show page references after internal links.
#latex_show_pagerefs = False

# If true, show URL addresses after external links.
#latex_show_urls = False

# Documents to append as an appendix to all manuals.
#latex_appendices = []

# If false, no module index is generated.
#latex_domain_indices = True


# -- Options for manual page output -------------------------------------------

# One entry per manual page. List of tuples
# (source start file, name, description, authors, manual section).
man_pages = [
    ('index', 'qcobj', u'QCOBJ Documentation',
     [u'Roberto Vidmar, Nicola Creati'], 1)
]

# If true, show URL addresses after external links.
#man_show_urls = False


# -- Options for Texinfo output -----------------------------------------------

# Grouping the document tree into Texinfo files. List of tuples
# (source start file, target name, title, author,
#  dir menu entry, description, category)
texinfo_documents = [
  ('index', 'QCOBJ', u'QCOBJ Documentation',
   u'Roberto Vidmar, Nicola Creati', 'QCOBJ',
   'A quantity aware version of Configbject.',
   'Miscellaneous'),
]

# Documents to append as an appendix to all manuals.
#texinfo_appendices = []

# If false, no module index is generated.
#texinfo_domain_indices = True

# How to display URL addresses: 'footnote', 'no', or 'inline'.
#texinfo_show_urls = 'footnote'
autosummary_generate = True

#~ nitpicky = True # use -n in Makefile instead

# http://sphinx.pocoo.org/theming.html
#autodoc_member_order = 'bysource'

intersphinx_mapping = {
        'configobj': ('https://configobj.readthedocs.io/en/latest/', None),
        'pyside2': ('https://doc.qt.io/qtforpython/', None),
        'pyside6': ('https://doc.qt.io/qtforpython/', None),
        'pint': ('https://pint.readthedocs.io/en/stable/', None)
        }
