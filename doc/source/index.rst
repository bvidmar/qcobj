.. _PySide2: https://pypi.org/project/PySide2
.. _PySide6: https://pypi.org/project/PySide6
.. _PyQt5: https://pypi.org/project/PyQt5
.. _Pint: http://pint.readthedocs.io/en/latest/

**********************************
QCOBJ Python Package Documentation
**********************************

Foreword
########
QCOBJ has been developed by two programmers working in the 
`Aerial Operations <http://www.inogs.it/en/content/aerial-operations>`_
group of the IRI Research Section at
`OGS - Istituto Nazionale di Oceanografia e di
Geofisica Sperimentale <http://www.inogs.it>`_.

Python is their favourite programming language since 2006.

The authors:

**Roberto Vidmar, Nicola Creati**

.. image:: images/vidmar.jpg
   :height: 134 px
   :width:  100 px 
.. image:: images/creati.jpg
   :height: 134 px
   :width:  100 px 

What is QCOBJ?
##############
Scientists often use configuration files (*cfg* files) to set the parameters
and              
initial conditions for their computer programs or simulations. When these       
parameters are not limited to numbers or strings but represent physical         
quantities their unit of measure must be taken into account.                    
Researchers are used to convert derived physical quantities by hand or          
with the help of some computer program but this operation slows down the
process        
and is inherently error prone.

**QCOBJ** tries                                 
to give an answer to this problem integrating unit of measure and hence   
dimensionality into parameters.
This approach ensures that programs using this package will always get        
numbers in the requested range and in the correct unit of measure               
independently of the units used in the configuration file.


QCOBJ in short
##############
  * create/edit configuration files with the power of
    `ConfigObj <http://configobj.readthedocs.io/en/latest/configobj.html>`_
  * mix physical quantities at your pleasure: specify pressure parameter
    like

    - pressure = 300.0 Pa *or*
    - pressure = 0.03 N / cm**2 *or*
    - pressure = 2.842 kgf / ft**2

    and let QCOBJ handle the conversion for you.
  * create validation files with physical quantities and valid range for
    all parameters.
  * use the validation file to define the preferred physical quantities
  * use :class:`qcobj.cfggui.CfgGui` to develop your own Qt based GUI to show/edit
    configuration files.

Dependencies
############
It relies on
`ConfigObj <http://configobj.readthedocs.io/en/latest/configobj.html>`_ and
Pint_ (PySide2_/PySide6_/PyQt5_, are optional for the gui).

Installation
############
QCOBJ can be installed from PyPi:

.. parsed-literal::                                                             
                                                                                
    pip3 install qcobj

but to take advantage of the GUI install it with 

.. parsed-literal::                                                             
                                                                                
    pip3 install qcobj[with_pyside2]

or    

.. parsed-literal::                                                             
                                                                                
    pip3 install qcobj[with_pyqt5]

For the bleeding edge install from bitbucket with the same options ([with..]):

.. parsed-literal::                                                             
                                                                                
    pip3 install git+https://bitbucket.org/bvidmar/qcobj

Credits
#######
This package was possible thanks to Hernan E. Grecco
<hernan.grecco@gmail.com> who released and
mantains Pint_ and helped in the integration.

.. note::
   To use the cfggui module please install either PySide2_ or PySide6_ or
   PyQt5_.
   
Contents
###############################################################################

.. toctree::
  :maxdepth: 2
  :numbered:
  :titlesonly:

  CfgGui widget <cfggui>
  A complex QCOBJ example file <fast14>
  A slightly differerent QCOBJ example file <fast19>
  QCOBJ Package Reference <qcobj>
  changes

.. warning::
   This code has been tested *only* on Linux Kubuntu 20.04.2LTS
   but should work also on Mac and Windows.

.. warning::
   This is work in progress!


Indices and Tables
##################
* :ref:`genindex`
* :ref:`modindex`
* :ref:`search` 
