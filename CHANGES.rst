Changes                                                                         
############################################################################### 
                                                                                
Version 1.2.7
******************************************************************************* 
- qtCompat module has been replaced by qtcompat package.
                                                                                
Version 1.2.6
******************************************************************************* 
- Updated qtCompat to work with PySide6. New environment variable
  QT_PREFERRED_LIB to select either PySide2 or PySide6 or PyQt5.

- cfqgui module now includes an executable main function.

